package com.jms.example.jms;

import java.util.concurrent.TimeUnit;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Consumer implements MessageListener {

	@Override
	@JmsListener(destination = "${spring.active-mq.topic}")
	@SneakyThrows
	public void onMessage(Message message) {
		ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
		log.info("Received Message: {}", amqMessage.getText());
		log.info("Going for work...");
		TimeUnit.MINUTES.sleep(1);
		log.info("Done ready for another one...");
	}

}
