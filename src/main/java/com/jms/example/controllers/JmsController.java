package com.jms.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jms.example.jms.Producer;

@Controller
public class JmsController {

	@Autowired
	Producer jmsProducer;

	@GetMapping("/jms/{message}")
	@ResponseBody
	public String sentMessage(@PathVariable String message) {
		jmsProducer.sendMessage(message);
		return "Sent";
	}
}
